package Modelo;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author NicoNi
 */
public class Negocio {

    private Caja caja;
    private ArrayList<Producto> productos = new ArrayList<>();
    private final CondicionTributaria condicionTributaria = CondicionTributaria.M;
    private Cliente cliente;
    private final int concepto = 1;
    private ArrayList<Rubro> rubros = new ArrayList<>();

    public void Negocio() {

    }

    public void Inicializar() {

        crearCaja(1);

        cliente = new Cliente("pepito", 11111111, TipoDni.DNI);

        Rubro r1 = new Rubro("comida", 1);
        Rubro r2 = new Rubro("bebida", 2);

        getRubros().add(r1);
        getRubros().add(r2);

        ArrayList<Producto> agregados1 = new ArrayList<>();
        ArrayList<Producto> agregados2 = new ArrayList<>();

        Producto a1 = new Producto(3, 15, "jamon", 100, true,r1.getNombre());
        Producto a2 = new Producto(4, 15, "queso", 60, true,r1.getNombre());
        Producto a3 = new Producto(5, 16, "huevo", 65, true,r1.getNombre());
        Producto b1 = new Producto(6, 50, "coca", 60, false,r2.getNombre());
        Producto a4 = new Producto(7, 0, "cebolla", 0, true,r1.getNombre());
        Producto a5 = new Producto(8, 0, "tomate", 0, true,r1.getNombre());

        agregados1.add(a1);
        agregados1.add(a2);
        agregados1.add(a3);
        agregados2.add(a5);

        productos.add(new Producto(1, 80, "sanguche comun", 60, agregados2, false,r1.getNombre()));
        productos.add(new Producto(2, 100, "sanguche especial", 60, agregados1, false,r1.getNombre()));
        productos.add(a1);
        productos.add(a2);
        productos.add(a3);
        productos.add(a4);
        productos.add(a5);
        productos.add(b1);
    }

    public void crearCaja(int id) {
        caja = new Caja();
        caja.setId(id);
        if (horaActual() < 15) {
            caja.setTurno("mañana");

        } else {
            caja.setTurno("noche");
        }
    }

    public int horaActual() {
        Date hora = new Date();
        String strDateFormat = "HH";
        SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);

        return Integer.parseInt(objSDF.format(hora));
    }

    public Caja getCaja() {
        return caja;
    }

    public void setCaja(Caja caja) {
        this.caja = caja;
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }

    public void setProductos(ArrayList<Producto> productos) {
        this.productos = productos;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public void crearVenta() {
        caja.crearVenta();
    }

    public void agregarClienteVtaCur() {
        caja.agregarClienteVtaCur(cliente);
    }

    public void checkTipoComprobante() {
        caja.checkTipoComprob(condicionTributaria);
    }

    public void agregarLinea(Producto prod,ArrayList<Producto> agregados2) {
        caja.agregarLinea(prod,agregados2);

    }

    public Double totalVentaCur() {
        return caja.totalVentaCur();
    }

    public void asignarCae(String cae) {
        caja.asignarCae(cae);
    }

    public void agregarVentaCur() {
        caja.agregarVentaCur();
    }

    public void dismStock() {
        caja.dismStock();
    }

    public double totalVentas() {
        return caja.totalVentas();
    }

    public int getConcepto() {
        return concepto;
    }

    /**
     * @return the rubros
     */
    public ArrayList<Rubro> getRubros() {
        return rubros;
    }

    /**
     * @param rubros the rubros to set
     */
    public void setRubros(ArrayList<Rubro> rubros) {
        this.rubros = rubros;
    }

    public void agregarProducto(ArrayList<String> prodParaAgregar, Producto prod,ArrayList<String> agregadosAfacturar) {
        ArrayList<Producto> agregados = new ArrayList<>();
        ArrayList<Producto> agregados2 = new ArrayList<>();
        for (int i = 0; i < prodParaAgregar.size(); i++) {
            for (Producto producto : productos) {
                if (prodParaAgregar.get(i).equals(producto.getNombre())) {
                    agregados.add(producto);
                }
            }
        }
        for (int i = 0; i < agregadosAfacturar.size(); i++) {
            for (Producto producto : productos) {
                if (agregadosAfacturar.get(i).equals(producto.getNombre())) {
                    agregados2.add(producto);
                }
            }
        }
        prod.setAgregados(agregados);
        
        agregarLinea(prod,agregados2);
    }

}
