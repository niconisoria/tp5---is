/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author JorgeA
 */
public class LineaVenta {
    
    private Producto prod;
    
  

    public LineaVenta(Producto prod) {
        this.prod = prod;
    }

    public Producto getProd() {
        return prod;
    }

    public void setProd(Producto prod) {
        this.prod = prod;
    }
   
}
