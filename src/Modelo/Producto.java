/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author JorgeA
 */
public class Producto {

    private int id;
    private double precio;
    private String nombre;
    private int cant;
    private ArrayList<Producto> agregados = new ArrayList<>();
    private Boolean esAgregado;
    private String rubro;

    public Producto(int id, double precio, String nombre, int cant, Boolean esAgregado, String rubro) {
        this.id = id;
        this.precio = precio;
        this.nombre = nombre;
        this.cant = cant;
        this.esAgregado = esAgregado;
        this.rubro = rubro;

    }

    public String getRubro() {
        return rubro;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    public Producto(int id, double precio, String nombre, int cant, ArrayList<Producto> agregados, Boolean esAgregado,String rubro) {
        this.id = id;
        this.precio = precio;
        this.nombre = nombre;
        this.cant = cant;
        this.agregados = agregados;
        this.rubro = rubro;
        this.esAgregado = esAgregado;
        

    }
  

    public ArrayList<Producto> getAgregados() {
        return agregados;
    }

    public void setAgregados(ArrayList<Producto> agregados) {
        this.agregados = agregados;
    }

    public Boolean getEsAgregado() {
        return esAgregado;
    }

    public void setEsAgregado(Boolean esAgregado) {
        this.esAgregado = esAgregado;
    }

   

    public int getCant() {
        return cant;
    }

    public void setCant(int cant) {
        this.cant = cant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public String toString() {
        return "Producto{" + "id=" + id + ", precio=" + precio + ", nombre=" + nombre + ", cant=" + cant + ", agregados=" + agregados + ", esAgregado=" + esAgregado + ", rubro=" + rubro + '}';
    }

    

}
