/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author JorgeA
 */
public class Caja {
    
    private Venta vtaCur;
    private ArrayList<Venta> ventas = new ArrayList<>();
    private int id; //hasta cuatro digito;
    private String turno;

    public Caja(int id, String turno) {
        this.id = id;
        this.turno = turno;
    }

    public Caja() {
        
    }
    
    public void crearVenta(){
        this.vtaCur = new Venta();
    }
    
    public void agregarLinea(Producto prod,ArrayList<Producto> agregados2){
       vtaCur.crearLinea(prod,agregados2);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }
    
    public double totalVentaCur(){
        return this.vtaCur.total();
    }
    public double totalVentas(){
          double tot = 0;
        
          for (Venta venta : ventas) {
                tot += venta.total();
            }
        
        return tot;
    }
    public void asignarCae(String cae){
        vtaCur.setCae(cae);
    }

    public Venta getVtaCur() {
        return vtaCur;
    }

    public ArrayList<Venta> getVentas() {
        return ventas;
    }

    public void setVentas(ArrayList<Venta> ventas) {
        this.ventas = ventas;
    }
    
    public void agregarVentaCur(){
        ventas.add(vtaCur);
    }
    
    public void agregarClienteVtaCur(Cliente cliente){
        vtaCur.setCliente(cliente);
    }
    
    public void checkTipoComprob(CondicionTributaria condTributaria){
        vtaCur.checkTipoComprob(condTributaria);
    }
    
    public void dismStock(){
        vtaCur.dismStock();
    }
}
