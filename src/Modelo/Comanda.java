/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;

/**
 *
 * @author ritorto
 */
public class Comanda {
    private ArrayList<Producto> productosComanda = new ArrayList<>();

    public ArrayList<Producto> getProductosComanda() {
        return productosComanda;
    }

    public void setProductosComanda(ArrayList<Producto> productosComanda) {
        this.productosComanda = productosComanda;
    }
   public void agregarProducto(Producto prod){
       productosComanda.add(prod);
   }
}
