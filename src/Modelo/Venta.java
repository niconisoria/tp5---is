/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author JorgeA
 */
public class Venta {

    private ArrayList<LineaVenta> lineas = new ArrayList<>();

    private String cae;
    private Date fecha;
    private Double ImpOpEx;
    private Double ImpTotConc;
    private Double ImpNeto;
    private Double ImpIVA;
    private Double ImpTrib;
    private Cliente cliente;
    private TipoComprob tipoComprob;
    private Comanda comanda;

    public Venta() {
        this.fecha = new Date();
        this.comanda = new Comanda();
    }

    public void crearLinea(Producto prod,ArrayList<Producto> agregados2) {
        LineaVenta lv = new LineaVenta(prod);
        lineas.add(lv);
        for (Producto agregado : agregados2) {
            if (agregado.getPrecio() != 0) {
                lineas.add(new LineaVenta(agregado));
            }
        }
        comanda.agregarProducto(prod);
        
    }

    public double total() {
        ImpNeto = 0.0;
        for (LineaVenta linea : lineas) {
            ImpNeto += linea.getProd().getPrecio();
        }
        
        return ImpNeto;
    }

    public ArrayList<LineaVenta> getLineas() {
        return lineas;
    }

    public void setLineas(ArrayList<LineaVenta> lineas) {
        this.lineas = lineas;
    }

    public String getCae() {
        return cae;
    }

    public void setCae(String cae) {
        this.cae = cae;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public TipoComprob getTipoComprob() {
        return tipoComprob;
    }

    public void setTipoComprob(TipoComprob tipoComprob) {
        this.tipoComprob = tipoComprob;
    }

    public Double getImpTotConc() {
        return ImpTotConc;
    }

    public void setImpTotConc(Double ImpTotConc) {
        this.ImpTotConc = ImpTotConc;
    }

    public Double getImpNeto() {
        return ImpNeto;
    }

    public void setImpNeto(Double ImpNeto) {
        this.ImpNeto = ImpNeto;
    }

    public Double getImpIVA() {
        return ImpIVA;
    }

    public void setImpIVA(Double ImpIVA) {
        this.ImpIVA = ImpIVA;
    }

    public Double getImpTrib() {
        return ImpTrib;
    }

    public void setImpTrib(Double ImpTrib) {
        this.ImpTrib = ImpTrib;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Double getImpOpEx() {
        return ImpOpEx;
    }

    public void setImpOpEx(Double ImpOpEx) {
        this.ImpOpEx = ImpOpEx;
    }
    
    public void dismStock(){
         for (LineaVenta linea : lineas) {
            linea.getProd().setCant(linea.getProd().getCant() - 1);
        }
    }
    
    public void checkTipoComprob(CondicionTributaria condicionTributaria){
        switch(condicionTributaria){
            case RI:
                if(cliente.getCondicionTributaria().getValue() == 1){
                    tipoComprob = TipoComprob.A;
                }
                else if(cliente.getCondicionTributaria().getValue() == 2){
                    tipoComprob = TipoComprob.B;
                }
                else if(cliente.getCondicionTributaria().getValue() == 3){
                    tipoComprob = TipoComprob.B;
                }
                break;
            case M:
                tipoComprob = TipoComprob.C;
                ImpOpEx = 0.0;
                ImpTotConc = 0.0;
                ImpNeto = 0.0;
                ImpIVA = 0.0;
                ImpTrib = 0.0;
                break;
        }
    }

    public Comanda getComanda() {
        return comanda;
    }

    public void setComanda(Comanda comanda) {
        this.comanda = comanda;
    }
    
}
