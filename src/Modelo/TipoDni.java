/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author ritorto
 */
public enum TipoDni {
    DNI(96),
    CUIT(80), 
    CUIL(86);
    
    private final int value;

    private TipoDni(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

   
    
    
}
