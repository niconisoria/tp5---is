/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author ritorto
 */
public enum CondicionTributaria {
    RI(1), 
    M(2),
    CF(3);
    
    private final int value;

    private CondicionTributaria(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    
}
