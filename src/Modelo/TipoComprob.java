/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author ritorto
 */
public enum TipoComprob {
    A(1), 
    B(6),
    C(11);
    private final int value;

    private TipoComprob(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
    
}
