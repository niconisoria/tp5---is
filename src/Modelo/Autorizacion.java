/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;
import fev1.dif.afip.gov.ar.ArrayOfFECAEDetRequest;
import fev1.dif.afip.gov.ar.DummyResponse;
import fev1.dif.afip.gov.ar.FECAECabRequest;
import fev1.dif.afip.gov.ar.FECAEDetRequest;
import fev1.dif.afip.gov.ar.FECAEResponse;
import fev1.dif.afip.gov.ar.FERecuperaLastCbteResponse;
import fev1.dif.afip.gov.ar.MonedaResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class Autorizacion {
    
    private long cuit;
    private String sign;
    private String token;
    
   
    public void Autorizar() {
        String codigo = "AE3FA828-1CAA-4BA8-AA2F-1FDB7E24972F";
        cuit = solicitarAutorizacion(codigo).getCuit();
        sign = solicitarAutorizacion(codigo).getSign().getValue();
        token = solicitarAutorizacion(codigo).getToken().getValue();
    }
    
    public String facturar(Venta venta, int ptoVta, int concepto) {
        String CAE = "";
        String AppServer = feDummy().getAppServer();
        String AuthServer = feDummy().getAuthServer();
        String DbServer = feDummy().getDbServer();
        
        Autorizar();
        
        if (AppServer.equals("OK") && AuthServer.equals("OK") && DbServer.equals("OK")) {
            fev1.dif.afip.gov.ar.FEAuthRequest auth = new fev1.dif.afip.gov.ar.FEAuthRequest();
            auth.setCuit(cuit);
            auth.setSign(sign);
            auth.setToken(token);
            
            fev1.dif.afip.gov.ar.FECAERequest feCAEReq = new fev1.dif.afip.gov.ar.FECAERequest();
            FECAECabRequest FeCabReq = new FECAECabRequest();
            ArrayOfFECAEDetRequest FeDetReq = new ArrayOfFECAEDetRequest();
            int CantReg = 1;
            int CbteTipo = venta.getTipoComprob().getValue();
            int PtoVta = ptoVta;
            
            FeCabReq.setCantReg(CantReg);
            FeCabReq.setCbteTipo(CbteTipo);
            FeCabReq.setPtoVta(PtoVta);
            List<FECAEDetRequest> FECAEDetRequest = FeDetReq.getFECAEDetRequest();
            FECAEDetRequest Comprobante = new FECAEDetRequest();
            int Concepto = concepto;
            int DocTipo = venta.getCliente().getTipoDni().getValue(); //corregir, hardcodeado 
            long DocNro = venta.getCliente().getDni();
            long CbteDesde = feCompUltimoAutorizado(auth, PtoVta, CbteTipo).getCbteNro() + 1;
            long CbteHasta = feCompUltimoAutorizado(auth, PtoVta, CbteTipo).getCbteNro() + 1;
            String CbteFch = date();
            Double ImpTotConc = venta.getImpTotConc();
            Double ImpOpEx = venta.getImpOpEx();
            Double ImpNeto = venta.getImpNeto();
            Double ImpIVA = venta.getImpIVA();
            Double ImpTrib = venta.getImpTrib();
            Double ImpTotal = ImpTotConc + ImpOpEx + ImpNeto + ImpIVA + ImpTrib;
            String MonId = feParamGetTiposMonedas(auth).getResultGet().getMoneda().get(0).getId();
            Double MonCotiz = 1.0;
            Comprobante.setConcepto(Concepto);
            Comprobante.setDocTipo(DocTipo);
            Comprobante.setDocNro(DocNro);
            Comprobante.setCbteDesde(CbteDesde);
            Comprobante.setCbteHasta(CbteHasta);
            Comprobante.setCbteFch(CbteFch);
            Comprobante.setImpTotConc(ImpTotConc);
            Comprobante.setImpOpEx(ImpOpEx);
            Comprobante.setImpNeto(ImpNeto);
            Comprobante.setImpIVA(ImpIVA);
            Comprobante.setImpTrib(ImpTrib);
            Comprobante.setImpTotal(ImpTotal);
            Comprobante.setMonId(MonId);
            Comprobante.setMonCotiz(MonCotiz);
            FECAEDetRequest.add(0, Comprobante);
            feCAEReq.setFeCabReq(FeCabReq);
            feCAEReq.setFeDetReq(FeDetReq);
            
            CAE = fecaeSolicitar(auth, feCAEReq).getFeDetResp().getFECAEDetResponse().get(0).getCAE();
        } else{
            CAE = "NO";
        }

        return CAE;
    }

    private static org.datacontract.schemas._2004._07.sge_service_contracts.Autorizacion solicitarAutorizacion(java.lang.String codigo) {
        org.tempuri.LoginService service = new org.tempuri.LoginService();
        org.tempuri.ILoginService port = service.getSGEAuthService();
        return port.solicitarAutorizacion(codigo);
    }


    private static String date() {
        Date objDate = new Date();
        String strDateFormat = "yyyyMMdd";
        SimpleDateFormat objSDF = new SimpleDateFormat(strDateFormat);
        return objSDF.format(objDate);
    }
    
    private static DummyResponse feDummy() {
        fev1.dif.afip.gov.ar.Service service = new fev1.dif.afip.gov.ar.Service();
        fev1.dif.afip.gov.ar.ServiceSoap port = service.getServiceSoap();
        return port.feDummy();
    }
    
    public static String dummy(){
        return feDummy().getAppServer();
    }
    
    private static FECAEResponse fecaeSolicitar(fev1.dif.afip.gov.ar.FEAuthRequest auth, fev1.dif.afip.gov.ar.FECAERequest feCAEReq) {
        fev1.dif.afip.gov.ar.Service service = new fev1.dif.afip.gov.ar.Service();
        fev1.dif.afip.gov.ar.ServiceSoap port = service.getServiceSoap();
        return port.fecaeSolicitar(auth, feCAEReq);
    }

    private static FERecuperaLastCbteResponse feCompUltimoAutorizado(fev1.dif.afip.gov.ar.FEAuthRequest auth, int ptoVta, int cbteTipo) {
        fev1.dif.afip.gov.ar.Service service = new fev1.dif.afip.gov.ar.Service();
        fev1.dif.afip.gov.ar.ServiceSoap port = service.getServiceSoap();
        return port.feCompUltimoAutorizado(auth, ptoVta, cbteTipo);
    }

    private static MonedaResponse feParamGetTiposMonedas(fev1.dif.afip.gov.ar.FEAuthRequest auth) {
        fev1.dif.afip.gov.ar.Service service = new fev1.dif.afip.gov.ar.Service();
        fev1.dif.afip.gov.ar.ServiceSoap port = service.getServiceSoap();
        return port.feParamGetTiposMonedas(auth);
    }
    
}
