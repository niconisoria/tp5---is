package Controlador;

import Modelo.Negocio;
import Vista.FinTurno;



public class ControladorTurno  {

    private FinTurno ft = new FinTurno();
  

    public ControladorTurno() {
        ft.setVisible(true);
        ft.setLocationRelativeTo(null);
     
    }

    public void finTurno(Negocio negocio) {
        ft.labTot.setText(String.valueOf(negocio.totalVentas()));

    }

   

}
