package Controlador;

import Modelo.Autorizacion;
import Modelo.LineaVenta;
import Modelo.Negocio;
import Modelo.Producto;
import Modelo.Rubro;
import Modelo.Venta;
import Vista.VistaPedidos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class ControladorPedidos implements ActionListener {

    private final VistaPedidos vp = new VistaPedidos();
    private Negocio negocio = new Negocio();
    private Autorizacion autorizacion = new Autorizacion();
    private DefaultTableModel modelo = (DefaultTableModel) vp.tPedido.getModel();
    private DefaultListModel modelo2 = new DefaultListModel();
    ArrayList<String> agregadosAfacturar = new ArrayList<>();

    public ControladorPedidos() {
        negocio.Inicializar();
        vp.btnNuevo.addActionListener(this);
        vp.btnAgAg.addActionListener(this);
        vp.btnAgCo.addActionListener(this);
        vp.btnConfi.addActionListener(this);
        vp.btnFin.addActionListener(this);
        vp.btnFinTur.addActionListener(this);
        vp.comboRu.addActionListener(this);
        vp.comboCo.addActionListener(this);
        vp.listaAg.setModel(modelo2);
        vp.btnQuit.addActionListener(this);
    }

    public void iniciar() {

        vp.setVisible(true);
        vp.setLocationRelativeTo(null);

        for (Rubro rubro : negocio.getRubros()) {
            vp.comboRu.addItem(rubro.getNombre());
        }

        vp.btnAgAg.setEnabled(false);
        vp.btnAgCo.setEnabled(false);
        vp.comboCo.setEnabled(false);
        vp.comboAg.setEnabled(false);
        vp.comboRu.setEnabled(false);

    }

    public void agregarProducto(String nombreProd) {
        Producto produ = null;
        boolean band = false;
        ArrayList<String> prodParaAgregar = new ArrayList<>();
        String nombreCo = nombreProd;
        limpiarTabla();
        for (Producto prod : negocio.getProductos()) {
            if (prod.getNombre().equals(nombreCo)) {
                for (int i = 0; i < modelo2.getSize(); i++) {
                    prodParaAgregar.add(String.valueOf(modelo2.getElementAt(i)));
                }
                produ = new Producto(prod.getId(), prod.getPrecio(), prod.getNombre(), prod.getCant(), prod.getAgregados(), prod.getEsAgregado(), prod.getRubro());
                negocio.agregarProducto(prodParaAgregar, produ,agregadosAfacturar);
                
            }
        }
        for (LineaVenta linea : negocio.getCaja().getVtaCur().getLineas()) {
            Object nuevo[] = {linea.getProd().getId(), linea.getProd().getNombre(), linea.getProd().getPrecio()};
            modelo.addRow(nuevo);
            
        }
        
        

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vp.btnNuevo) {

            vp.btnAgAg.setEnabled(true);
            vp.btnAgCo.setEnabled(true);
            vp.comboCo.setEnabled(true);
            vp.comboRu.setEnabled(true);
            vp.comboAg.setEnabled(true);
            vp.btnFin.setEnabled(false);

            negocio.crearVenta();
            negocio.agregarClienteVtaCur();
            negocio.checkTipoComprobante();

            vp.btnNuevo.setEnabled(false);
            vp.lbCae.setText("");
            vp.lbTotal.setText("");

        }
        if (e.getSource() == vp.btnAgCo) {
            agregarProducto((String) vp.comboCo.getSelectedItem());
            agregadosAfacturar.clear();
            modelo2.clear();
            for (Producto producto : negocio.getProductos()) {
                
                if (producto.getNombre().equals(vp.comboCo.getSelectedItem())) {
                    for (Producto agregado : producto.getAgregados()) {
                        modelo2.addElement(agregado.getNombre());
                    }
                }

            }
            
        }

        if (e.getSource() == vp.btnConfi) {

            vp.lbTotal.setText(String.valueOf(negocio.totalVentaCur()));
            vp.btnFin.setEnabled(true);

        }
        if (e.getSource() == vp.btnFin) {

            negocio.asignarCae(autorizacion.facturar(negocio.getCaja().getVtaCur(), negocio.getCaja().getId(), negocio.getConcepto()));
            negocio.agregarVentaCur();
            vp.lbCae.setText(negocio.getCaja().getVtaCur().getCae());
            negocio.dismStock();

            limpiarTabla();
            vp.btnNuevo.setEnabled(true);
            vp.btnFin.setEnabled(false);

        }
        if (e.getSource() == vp.btnFinTur) {

            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "¿Desea Finalizar el Turno?", "Fin De Turno", dialogButton);
            if (dialogResult == 0) {
                ControladorTurno ct = new ControladorTurno();
                ct.finTurno(negocio);
            } else {
                System.out.println("No Option");
            }
        }
        if (e.getSource() == vp.comboRu) {
            vp.comboCo.setEnabled(true);
            vp.comboCo.removeAllItems();
            for (Producto producto : negocio.getProductos()) {
                if (String.valueOf((vp.comboRu.getSelectedItem())).equals(producto.getRubro()) && !producto.getEsAgregado()) {
                    vp.comboCo.addItem(producto.getNombre());
                }
            }
        }
        if (e.getSource() == vp.comboCo) {
            vp.comboAg.setEnabled(true);
            vp.comboAg.removeAllItems();
            modelo2.clear();
            agregadosAfacturar.clear();

            for (Producto producto : negocio.getProductos()) {
                if (producto.getEsAgregado() == true) {
                    vp.comboAg.addItem(producto.getNombre());

                }
                if (producto.getNombre().equals(vp.comboCo.getSelectedItem())) {
                    for (Producto agregado : producto.getAgregados()) {
                        modelo2.addElement(agregado.getNombre());
                    }
                }

            }
        }
        if (e.getSource() == vp.btnAgAg) {
            
            modelo2.addElement(String.valueOf(vp.comboAg.getSelectedItem()));
            agregadosAfacturar.add(String.valueOf(vp.comboAg.getSelectedItem()));

        }
        if (e.getSource() == vp.btnQuit) {
            for (String agregado : agregadosAfacturar) {
                if (agregado.equals(String.valueOf(vp.listaAg.getSelectedValue()))) {
                    agregadosAfacturar.remove(agregado);
                    break;
                }
            }
            modelo2.remove(vp.listaAg.getSelectedIndex());

        }
    }
    public void limpiarTabla(){
        for (int i = modelo.getRowCount() - 1; i >= 0; i--) {
                modelo.removeRow(i);
            }
    }
}
