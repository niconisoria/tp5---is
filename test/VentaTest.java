/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import Modelo.Producto;
import Modelo.Venta;
import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ritorto Alvaro
 */
public class VentaTest {
  
    public VentaTest() {
               

    }
    
    @Test
    public void testTotal1(){
        Venta v = new Venta();
        Producto p1 = new Producto(1, 100, "sanguche comun", 1, false, "Comidas");
        v.crearLinea(p1, new ArrayList <Producto> ());
        
        v.total();
        
        Assert.assertEquals(100, v.getImpNeto(),0.01);

    }
    
    @Test
    public void testTotal2(){
        
        Venta v = new Venta();
         
        Producto p1 = new Producto(1, 100, "sanguche comun", 1, false, "Comidas");
        Producto a1 = new Producto(2, 5, "huevo", 1, true, "agregados");
        
        ArrayList<Producto> Productos = new ArrayList<Producto>();
        Productos.add(a1);
        
        v.crearLinea(p1, Productos);
        
        v.total();
        
        Assert.assertEquals(105, v.getImpNeto(),0.01);

    }
    
    @Test
    public void testTotal3(){
        
        Venta v = new Venta();
         
        Producto p1 = new Producto(1, 100, "sanguche comun", 1, false, "Comidas");
        Producto a1 = new Producto(2, 5, "huevo", 1, true, "agregados");
        Producto a2 = new Producto(3, 8, "queso", 1, true, "agregados");
        ArrayList<Producto> Productos = new ArrayList<Producto>();
        Productos.add(a1);
        Productos.add(a2);
        v.crearLinea(p1, Productos);
        
        v.total();
        
        Assert.assertEquals(113, v.getImpNeto(),0.01);

    }
    
    @Test
    public void testDismStock(){
        Venta v = new Venta();
        Producto p1 = new Producto(1, 100, "sanguche comun", 1, false, "Comidas");
        v.crearLinea(p1, new ArrayList <Producto> ());
        v.dismStock();
        Assert.assertEquals(0, p1.getCant());
    }
    
    @Test
    public void testDismStock2(){
        
        Venta v = new Venta();
         
        Producto p1 = new Producto(1, 100, "sanguche comun", 1, false, "Comidas");
        Producto a1 = new Producto(2, 5, "huevo", 1, true, "agregados");
        Producto a2 = new Producto(3, 8, "queso", 1, true, "agregados");
        ArrayList<Producto> Productos = new ArrayList<Producto>();
        Productos.add(a1);
        Productos.add(a2);
        v.crearLinea(p1, Productos);
        
        v.dismStock();
        
        Assert.assertEquals(0, p1.getCant());
        Assert.assertEquals(0, a1.getCant());
        Assert.assertEquals(0, a2.getCant());

    }
//    
//    public void dismStock(){
//         for (LineaVenta linea : lineas) {
//            linea.getProd().setCant(linea.getProd().getCant() - 1);
//        }
//    }
//      public double total() {
//        ImpNeto = 0.0;
//        for (LineaVenta linea : lineas) {
//            ImpNeto += linea.getProd().getPrecio();
//        }
//        
//        return ImpNeto;
//    }
    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
